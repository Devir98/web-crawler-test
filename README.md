# README #

This project is my solution to the BuildIt web-crawler-test. It is a spring boot application with one RESTful API. 

### How to run the application? ###

The application can build itself using an included gradle wrapper and can be built/tested using:

./gradlew clean build

The application can then be ran using(will use port 8080 by default):

./gradlew bootRun

You can send a request to the API with this example:

curl -H "Content-Type: application/json" --request POST \
 --url http://localhost:8080/extract \
 --data 'http://www.google.co.uk/'

### Constraints ###

There is no limit to the depth searched through, the application will not search through duplicate pages because if there was a cyclical dependency, it would never finish. If a large web site is selected, it will keep going.

Ideally, I would have also included some multi-threading and RXJava so I could search through child pages at the same time and compile the result after but I didnt want to exceed the 2 hours.

I would have preferred using a 3rd party library to handle the file extensions where I did not want to search for a link because it wasn't a document. I manually created the filter and I know that it is not a complete list.

Another improvement I would make is on the data type inputted, I think a wrapper object with also a depth field to go with the URL would improve the usability.