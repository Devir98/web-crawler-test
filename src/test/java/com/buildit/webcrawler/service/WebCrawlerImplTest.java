package com.buildit.webcrawler.service;

import com.buildit.webcrawler.domain.Resource;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.net.URISyntaxException;
import java.util.Map;

import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Jsoup.class)
public class WebCrawlerImplTest {

    private static final String url = "https://buildit.digital/";
    private static final String differentUrl = "http://www.google.co.uk";
    private WebCrawlerImpl webCrawler;
    private Connection mockConnection;
    private Document mockDocument;
    private Elements mockElements;

    @Before
    public void setup() {
        webCrawler = new WebCrawlerImpl();

        PowerMockito.mockStatic(Jsoup.class);

        mockConnection = mock(Connection.class);
        mockDocument = mock(Document.class);
        mockElements = mock(Elements.class);

    }

    @Test
    public void shouldTestSameDomainTrue() throws URISyntaxException {
        //Given
        boolean result = WebCrawlerImpl.isSameDomain(url, url);

        //Then
        assertTrue(result);
    }

    @Test
    public void shouldTestSameDomainFalse() throws URISyntaxException {
        //Given
        boolean result = WebCrawlerImpl.isSameDomain(url, differentUrl);

        //Then
        assertFalse(result);
    }

    @Test
    public void getLinksForUrl() throws Exception {
        //Given
        when(Jsoup.connect(anyString())).thenReturn(mockConnection);
        when(mockConnection.get()).thenReturn(mockDocument);
        when(mockDocument.select(anyString())).thenReturn(mockElements);
        when(mockElements.attr(anyString())).thenReturn(url + "test/");

        Resource result = webCrawler.getLinksForUrl(url);

        //Then
        assertNotNull(result);
        assertThat(result.getUrl(), is("https://buildit.digital/"));
        assertNotNull(result.getChildren());
        assertTrue(result.getChildren().isEmpty());
    }

    @Test
    public void extractLinksFromResource() throws Exception {
        //Given
        when(Jsoup.connect(anyString())).thenReturn(mockConnection);
        when(mockConnection.get()).thenReturn(mockDocument);
        when(mockDocument.select(anyString())).thenReturn(mockElements);
        when(mockElements.attr(anyString())).thenReturn(url + "test/");

        Resource testParent = new Resource();
        testParent.setUrl(url);
        Resource child1 = new Resource();
        child1.setUrl("test2/");
        testParent.getChildren().put(url + "test2/", child1);

        Map<String, Resource> results = webCrawler.extractLinksFromResource(testParent);

        //Then
        assertNotNull(results);
        assertThat(results.size(), is(1));
    }

    @Test
    public void processUniqueLinks() throws Exception {
        //Given
        when(Jsoup.connect(anyString())).thenReturn(mockConnection);
        when(mockConnection.get()).thenReturn(mockDocument);
        when(mockDocument.select(anyString())).thenReturn(mockElements);
        when(mockElements.attr(anyString())).thenReturn(url + "test/");

        Resource testParent = new Resource();
        testParent.setUrl(url);
        Resource child1 = new Resource();
        child1.setUrl(url + "test2/");
        Resource child2 = new Resource();
        child2.setUrl(url + "test3/");
        testParent.getChildren().put(url + "test2/", child1);
        testParent.getChildren().put(url + "test3/", child2);

        Map<String, Resource> results = webCrawler.processUniqueLinks(testParent, newHashSet(child1.getUrl(), child2.getUrl()));

        //Then
        assertNotNull(results);
        assertThat(results.size(), is(2));

        //Given
        Resource child3 = new Resource();
        child3.setUrl(url + "test4/");
        testParent.getChildren().put(url + "test4/", child3);

        results = webCrawler.processUniqueLinks(testParent, newHashSet(child1.getUrl(), child2.getUrl(), child3.getUrl()));

        //Then
        assertNotNull(results);
        assertThat(results.size(), is(3));
    }

    @Test
    public void stringContainsExt() throws Exception {
        //Given
        boolean result = WebCrawlerImpl.stringContainsExt("http://test.jpg");

        //Then
        assertTrue(result);

        //Given
        result = WebCrawlerImpl.stringContainsExt("http://test.html");

        //Then
        assertFalse(result);
    }

}