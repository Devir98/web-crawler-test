package com.buildit.webcrawler.controller;

import com.buildit.webcrawler.service.WebCrawler;
import com.buildit.webcrawler.service.WebCrawlerImpl;
import com.buildit.webcrawler.domain.Resource;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WebCrawlerControllerTest {

    private WebCrawlerController webCrawlerController;
    private static final String url = "https://buildit.digital/";

    @Before
    public void setup() throws Exception{
        WebCrawler webCrawler = mock(WebCrawlerImpl.class);

        Resource testParent = new Resource();
        testParent.setUrl(url);
        Resource child1 = new Resource();
        child1.setUrl(url + "test2/");
        Resource child2 = new Resource();
        child2.setUrl(url + "test3/");
        testParent.getChildren().put(url + "test2/", child1);
        testParent.getChildren().put(url + "test3/", child2);

        when(webCrawler.getLinksForUrl(anyString())).thenReturn(testParent);

        webCrawlerController = new WebCrawlerController(webCrawler);
    }

    @Test
    public void shouldProcessReturnUrls() throws Exception {
        //Given
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        DeferredResult<String> result = webCrawlerController.extract(url, mockRequest, mockResponse);

        //Then
        assertNotNull(result);
        assertTrue(result.getResult().equals("https://buildit.digital/\n" +
                "-https://buildit.digital/test2/\n" +
                "-https://buildit.digital/test3/\n"));
    }

}