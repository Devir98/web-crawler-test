package com.buildit.webcrawler.service;

import com.google.common.annotations.VisibleForTesting;
import com.buildit.webcrawler.domain.Resource;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

@Component
public class WebCrawlerImpl implements WebCrawler {

    //TODO should find a 3rd party library to handle filtering file extensions, not a complete list
    private static String[] extensions = new String[]{".jpg", ".pdf", ".png"};
    private Map<String, Resource> resourcesProcessed = new HashMap<>();

    public WebCrawlerImpl() {
        this.resourcesProcessed = newHashMap();
    }

    @VisibleForTesting
    static boolean isSameDomain(String baseUrl, String url) {

        try {
            URI baseUri = new URI(baseUrl);

            String baseDomain = baseUri.getHost();

            URI uri = new URI(url);
            String domain = uri.getHost();

            return baseDomain.equals(domain);
        } catch (URISyntaxException e) {
            System.out.println("An error occurred when extracting checking the domain for URL: " + url + ", " + e.getMessage());
        }

        return false;
    }

    public Resource getLinksForUrl(String url) throws IOException {

        System.out.println("Finding links for url: " + url);
        if (isNullOrEmpty(url)) {
            return new Resource();
        }


        Resource parent = new Resource();
        parent.setUrl(url);

        extractLinksFromResource(parent);

        return parent;
    }

    @VisibleForTesting
    Map<String, Resource> extractLinksFromResource(Resource currentResource) {

        System.out.println("Extracting links for resource:" + currentResource.getUrl());
        resourcesProcessed.put(currentResource.getUrl(), currentResource);

        Set<String> urls = newHashSet();

        Document doc;

        try {
            doc = Jsoup.connect(currentResource.getUrl()).get();

            Elements links = doc.select("a[href]");

            links.forEach(link -> urls.add(link.attr("abs:href")));

            return processUniqueLinks(currentResource, urls);
        } catch (IOException e) {
            System.out.println("An error occurred when extracting the DOM for URL: " + currentResource.getUrl() + ", " + e.getMessage());
        }

        return resourcesProcessed;
    }

    @VisibleForTesting
    Map<String, Resource> processUniqueLinks(Resource parentResource, Set<String> urls) {

        urls.forEach(url -> {
            if (resourcesProcessed.containsKey(url) || !isSameDomain(parentResource.getUrl(), url) ||
                    stringContainsExt(url)) {
                //Not going to reprocess duplicates or links that are not documents
                parentResource.getChildren().put("url", null);
            } else {
                Resource currentResource = new Resource();
                currentResource.setUrl(url);
                parentResource.getChildren().put(url, currentResource);

                extractLinksFromResource(currentResource);
            }
        });

        return resourcesProcessed;
    }

    @VisibleForTesting
    static boolean stringContainsExt(String input) {
        return Arrays.stream(extensions).parallel().anyMatch(input::contains);
    }
}
