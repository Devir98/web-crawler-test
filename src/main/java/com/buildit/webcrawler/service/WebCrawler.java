package com.buildit.webcrawler.service;

import com.buildit.webcrawler.domain.Resource;

import java.io.IOException;

public interface WebCrawler {

    Resource getLinksForUrl(String url) throws IOException;
}
