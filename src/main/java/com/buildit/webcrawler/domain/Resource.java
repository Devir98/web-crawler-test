package com.buildit.webcrawler.domain;

import lombok.Data;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@Data
public class Resource {

    private String url;
    private Map<String, Resource> children = newHashMap();

}
