package com.buildit.webcrawler.controller;

import com.buildit.webcrawler.service.WebCrawler;
import com.google.common.annotations.VisibleForTesting;
import com.buildit.webcrawler.domain.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.repeat;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

@Controller
public class WebCrawlerController {

    private WebCrawler webCrawler;

    //I prefer to autowire a constructor rather than private variables to make testing easier
    @Autowired
    public WebCrawlerController(WebCrawler webCrawler) {
        this.webCrawler = webCrawler;
    }

    @VisibleForTesting
    static StringBuilder parseResults(Resource parent, final int depth, StringBuilder stringBuilder) {
        if (depth == 0) {
            stringBuilder.append(parent.getUrl()).append("\n");
        }

        parent.getChildren().values().stream()
                .filter(Objects::nonNull)
                .forEach(child -> {
                    stringBuilder.append(repeat("-", depth + 1)).append(child.getUrl()).append("\n");
                    if (!child.getChildren().isEmpty()) {
                        parseResults(child, depth + 1, stringBuilder);
                    }
                });

        return stringBuilder;
    }

    @RequestMapping(value = "/extract", method = RequestMethod.POST)
    @ResponseBody
    DeferredResult<String> extract(@RequestBody String url, HttpServletRequest request, HttpServletResponse response) {

        //Should set timeout here
        DeferredResult<String> deferredResult = new DeferredResult<>();

        try {
            Resource result = webCrawler.getLinksForUrl(url);

            StringBuilder parsedResult = parseResults(result, 0, new StringBuilder());

            response.setStatus(OK.value());
            deferredResult.setResult(parsedResult.toString());
        } catch (IOException e) {
            response.setStatus(INTERNAL_SERVER_ERROR.value());
            deferredResult.setResult(e.getMessage());
        }

        return deferredResult;
    }
}
