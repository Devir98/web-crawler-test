package com.buildit.webcrawler;

import com.buildit.webcrawler.service.WebCrawler;
import com.buildit.webcrawler.service.WebCrawlerImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public WebCrawler webCrawler() {
        return new WebCrawlerImpl();
    }
}
